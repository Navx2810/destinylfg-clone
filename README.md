# Destiny LFG Clone for Learning Purposes #
* * *
This project will attempt to clone [DestinyLFG.net](http://www.destinylfg.net). This _clone_ is being created for the purpose of learning web tech.

## Functional Design ##
* * *
The website will allow users to create a listing to advertise that they are looking for more members or looking to join a party.

## Requirements ##
* * * 
The website will **_need_** to. . .

- Fetch and update information displayed in real-time __[ Note: I heard a mention of AJAX being used for this purpose ]__
- Allow users to add or remove a listing _(Only one listing at any given time)_